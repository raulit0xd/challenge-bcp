FROM adoptopenjdk/openjdk8-openj9:latest
RUN mkdir /opt/app
COPY build/libs/*-SNAPSHOT.jar /opt/app/microservice.jar
EXPOSE 8080
CMD ["/bin/sh", "-c", "java -jar /opt/app/microservice.jar --spring.profiles.active=h2"]