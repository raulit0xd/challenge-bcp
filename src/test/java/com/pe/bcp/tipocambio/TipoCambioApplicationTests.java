package com.pe.bcp.tipocambio;

import com.pe.bcp.tipocambio.rest.controller.CambioMonedaController;
import com.pe.bcp.tipocambio.rest.request.CambioMonedaRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

@SpringBootTest
class TipoCambioApplicationTests {

    @Autowired
    private CambioMonedaController cambioMonedaController;

    @Test
    void cuandoEnvioCambioDolaresASolesEntoncesRetornaValorMayorAMonto() {

        final CambioMonedaRequest cambioMonedaRequest = getCambioMonedaRequest("100.32", "S/", "$");
        Assertions.assertEquals(1, cambioMonedaController.create(cambioMonedaRequest)
                .getMontoTipoCambio().compareTo(cambioMonedaRequest.getMonto()));
    }

    @Test
    void cuandoActualizoTipoCambioDolaresASolesEntoncesRetornaMontoIgual300() {
        final CambioMonedaRequest cambioMonedaRequest = getCambioMonedaRequest("3.00", "S/", "$");
        cambioMonedaController.updateTipoCambio(cambioMonedaRequest);
        Assertions.assertEquals(0, cambioMonedaController.create(getCambioMonedaRequest("100", "S/", "$"))
                .getMontoTipoCambio().compareTo(new BigDecimal("300.00")));
    }

    private CambioMonedaRequest getCambioMonedaRequest(String monto, String monedaDestino, String monedaOrigen) {
        final CambioMonedaRequest cambioMonedaRequest = new CambioMonedaRequest();
        cambioMonedaRequest.setMonto(new BigDecimal(monto));
        cambioMonedaRequest.setMonedaDestino(monedaDestino);
        cambioMonedaRequest.setMonedaOrigen(monedaOrigen);
        return cambioMonedaRequest;
    }

}
