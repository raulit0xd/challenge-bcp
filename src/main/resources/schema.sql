create table IF NOT EXISTS moneda (
    id_moneda int(11) NOT NULL AUTO_INCREMENT,
    simbolo varchar(5) NOT NULL,
    descripcion varchar(50),
    activo bit(1) NOT NULL,
    PRIMARY KEY(id_moneda)

);

create table IF NOT EXISTS tipo_cambio (
    id_tipo_cambio int(11) NOT NULL AUTO_INCREMENT,
    valor_cambio decimal(18, 2) NOT NULL,
    id_moneda_origen int(11) NOT NULL,
    id_moneda_destino int(11) NOT NULL,
    fecha_cambio  varchar(12) ,
    activo bit(1) NOT NULL,
    PRIMARY KEY(id_tipo_cambio)
);
create table IF NOT EXISTS cambio_moneda (
    id_cambio_moneda int(11) NOT NULL AUTO_INCREMENT,
    monto decimal(18, 2) NOT NULL,
    monto_cambio decimal(18, 2) NOT NULL,
    id_tipo_cambio int(11) NOT NULL,
    fecha_hora datetime NOT NULL,
    PRIMARY KEY(id_cambio_moneda)
);