INSERT INTO moneda(id_moneda,simbolo,descripcion, activo) VALUES (1, 'S/','Soles', 1);
INSERT INTO moneda(id_moneda,simbolo,descripcion, activo) VALUES  (2, '$','Dolares', 1);
INSERT INTO moneda(id_moneda,simbolo,descripcion, activo) VALUES  (3, '€','Euros', 1);
INSERT INTO tipo_cambio(id_tipo_cambio,valor_cambio,id_moneda_origen,id_moneda_destino,activo,fecha_cambio) VALUES (1, 3.33, 2, 1, 1,'20210101');
INSERT INTO tipo_cambio(id_tipo_cambio,valor_cambio,id_moneda_origen,id_moneda_destino,activo,fecha_cambio) VALUES (7, 3.55, 2, 1, 1,'20210102');

INSERT INTO tipo_cambio(id_tipo_cambio,valor_cambio,id_moneda_origen,id_moneda_destino,activo,fecha_cambio) VALUES (2, 0.31, 1, 2, 1,'20210101');
INSERT INTO tipo_cambio(id_tipo_cambio,valor_cambio,id_moneda_origen,id_moneda_destino,activo,fecha_cambio) VALUES (3, 3.71, 3, 1, 1,'20210101');
INSERT INTO tipo_cambio(id_tipo_cambio,valor_cambio,id_moneda_origen,id_moneda_destino,activo,fecha_cambio) VALUES (4, 0.27, 1, 3, 1,'20210101');
INSERT INTO tipo_cambio(id_tipo_cambio,valor_cambio,id_moneda_origen,id_moneda_destino,activo,fecha_cambio) VALUES (5, 0.90, 2, 3, 1,'20210101');
INSERT INTO tipo_cambio(id_tipo_cambio,valor_cambio,id_moneda_origen,id_moneda_destino,activo,fecha_cambio) VALUES (6, 1.11, 3, 2, 1,'20210101');