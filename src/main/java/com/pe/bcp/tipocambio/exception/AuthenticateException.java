package com.pe.bcp.tipocambio.exception;

public class AuthenticateException extends Exception {
    public AuthenticateException(String userDisabled, Throwable e) {
        super(userDisabled, e);
    }
}
