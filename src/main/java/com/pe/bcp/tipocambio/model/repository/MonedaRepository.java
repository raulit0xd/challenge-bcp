package com.pe.bcp.tipocambio.model.repository;

import com.pe.bcp.tipocambio.model.entity.Moneda;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MonedaRepository extends JpaRepository<Moneda, Long> {

    Moneda findBySimbolo(String simbolo);
}
