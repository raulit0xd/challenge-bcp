package com.pe.bcp.tipocambio.model.repository;

import com.pe.bcp.tipocambio.model.entity.CambioMoneda;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CambioMonedaRepository extends JpaRepository<CambioMoneda, Long> {
}
