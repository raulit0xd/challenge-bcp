package com.pe.bcp.tipocambio.model.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Data
@Table(name = "tipo_cambio")
public class TipoCambio {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_tipo_cambio")
    private Long idTipoCambio;
    @Column(name = "valor_cambio")
    private BigDecimal valorCambio;
    @ManyToOne
    @JoinColumn(name = "id_moneda_origen")
    private Moneda monedaOrigen;
    @ManyToOne
    @JoinColumn(name = "id_moneda_destino")
    private Moneda monedaDestino;
    @Column(name = "activo")
    private Boolean activo;


    @Column(name = "fecha_cambio")
    private String fechaCambio;

}
