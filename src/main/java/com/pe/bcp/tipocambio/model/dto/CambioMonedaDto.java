package com.pe.bcp.tipocambio.model.dto;

import com.pe.bcp.tipocambio.rest.request.CambioMonedaRequest;
import lombok.*;

import java.math.BigDecimal;

@NoArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class CambioMonedaDto extends CambioMonedaRequest {

    public CambioMonedaDto(CambioMonedaRequest cambioMonedaRequest) {
        super(cambioMonedaRequest.getMonto(), cambioMonedaRequest.getMonedaOrigen(), cambioMonedaRequest.getMonedaDestino(),cambioMonedaRequest.getFechaCambio());
    }
    private BigDecimal montoTipoCambio;
    private BigDecimal tipoCambio;
}
