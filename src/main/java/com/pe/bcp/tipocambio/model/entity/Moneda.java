package com.pe.bcp.tipocambio.model.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "moneda")
public class Moneda {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_moneda")
    private Long idMoneda;
    @Column(name = "simbolo")
    private String simbolo;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "activo")
    private Boolean activo;
}
