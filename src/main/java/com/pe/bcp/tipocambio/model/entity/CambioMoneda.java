package com.pe.bcp.tipocambio.model.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "cambio_moneda")
@Data
public class CambioMoneda {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_cambio_moneda")
    private Long idCambioMoneda;
    @Column(name = "monto")
    private BigDecimal monto;
    @Column(name = "monto_cambio")
    private BigDecimal montoCambio;
    @ManyToOne
    @JoinColumn(name = "id_tipo_cambio")
    private TipoCambio tipoCambio;
    @Temporal(TemporalType.DATE)
    @Column(name = "fecha_hora")
    private Date fechaHora;
}
