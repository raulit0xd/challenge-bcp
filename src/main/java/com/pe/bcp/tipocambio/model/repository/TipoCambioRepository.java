package com.pe.bcp.tipocambio.model.repository;

import com.pe.bcp.tipocambio.model.entity.TipoCambio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface TipoCambioRepository extends JpaRepository<TipoCambio, Long> {

    TipoCambio findByActivoIsTrueAndMonedaOrigenSimboloAndMonedaDestinoSimboloAndFechaCambio(String simboloOrigen, String simboloDestino, String fechaCambio);
}
