package com.pe.bcp.tipocambio.service;

import com.pe.bcp.tipocambio.model.dto.CambioMonedaDto;
import com.pe.bcp.tipocambio.model.entity.CambioMoneda;
import com.pe.bcp.tipocambio.model.entity.TipoCambio;
import com.pe.bcp.tipocambio.model.repository.CambioMonedaRepository;
import com.pe.bcp.tipocambio.model.repository.TipoCambioRepository;
import com.pe.bcp.tipocambio.rest.request.CambioMonedaRequest;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;

@AllArgsConstructor
@Service
public class CambioMonedaService {

    private CambioMonedaRepository cambioMonedaRepository;
    private TipoCambioRepository tipoCambioRepository;

    public CambioMonedaDto realizarCambio(CambioMonedaRequest cambioMonedaRequest) {
        final TipoCambio tipoCambio = tipoCambioRepository
                .findByActivoIsTrueAndMonedaOrigenSimboloAndMonedaDestinoSimboloAndFechaCambio(cambioMonedaRequest.getMonedaOrigen(),
                        cambioMonedaRequest.getMonedaDestino(),cambioMonedaRequest.getFechaCambio());

        final CambioMonedaDto result = new CambioMonedaDto(cambioMonedaRequest);
        if(tipoCambio!=null) {
            result.setMontoTipoCambio(cambioMonedaRequest.getMonto().multiply(tipoCambio.getValorCambio()));
            result.setTipoCambio(tipoCambio.getValorCambio());
            tipoCambio.setFechaCambio(cambioMonedaRequest.getFechaCambio());
            CambioMoneda cambioMoneda = new CambioMoneda();
            cambioMoneda.setFechaHora(new Date());
            cambioMoneda.setMonto(result.getMonto());
            cambioMoneda.setMontoCambio(result.getMontoTipoCambio());
            cambioMoneda.setTipoCambio(tipoCambio);
            cambioMonedaRepository.save(cambioMoneda);
            return result;
        }else
            return new CambioMonedaDto();

    }

    public void actualizarTipoCambio(CambioMonedaRequest cambioMonedaRequest) {
        final TipoCambio tipoCambio = tipoCambioRepository
                .findByActivoIsTrueAndMonedaOrigenSimboloAndMonedaDestinoSimboloAndFechaCambio(cambioMonedaRequest.getMonedaOrigen(),
                                       cambioMonedaRequest.getMonedaDestino(),cambioMonedaRequest.getFechaCambio());
        tipoCambio.setValorCambio(cambioMonedaRequest.getMonto());
        tipoCambioRepository.save(tipoCambio);
    }
}
