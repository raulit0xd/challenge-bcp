package com.pe.bcp.tipocambio.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class JwtUserDetailsService implements UserDetailsService {

    @Override
    public UserDetails loadUserByUsername(String usuario) {
        if (usuario == null) {
            throw new UsernameNotFoundException("Usuario no encontrado: " + usuario);
        }
        return new org.springframework.security.core.userdetails.User(usuario, "123456",
                new ArrayList<>());
    }

}
