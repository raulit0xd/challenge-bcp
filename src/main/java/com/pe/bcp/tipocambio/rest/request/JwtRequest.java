package com.pe.bcp.tipocambio.rest.request;

import lombok.Data;

@Data
public class JwtRequest {

    private String usuario;
    private String contrasenia;
}
