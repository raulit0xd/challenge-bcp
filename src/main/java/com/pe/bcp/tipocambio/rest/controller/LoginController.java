package com.pe.bcp.tipocambio.rest.controller;

import com.pe.bcp.tipocambio.config.JwtToken;
import com.pe.bcp.tipocambio.exception.AuthenticateException;
import com.pe.bcp.tipocambio.rest.request.JwtRequest;
import com.pe.bcp.tipocambio.rest.response.JwtResponse;
import com.pe.bcp.tipocambio.service.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class LoginController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtToken jwtToken;

    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;


    @PostMapping(value = "/authenticate")
    public ResponseEntity<JwtResponse>
    createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws AuthenticateException {


        authenticate(authenticationRequest.getUsuario(), authenticationRequest.getContrasenia());

        final UserDetails userDetails = jwtUserDetailsService

                .loadUserByUsername(authenticationRequest.getUsuario());

        final String token = jwtToken.generateToken(userDetails);

        return ResponseEntity.ok(new JwtResponse(token));

    }

    private void authenticate(String username, String password) throws AuthenticateException {

        try {

            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));

        } catch (DisabledException e) {

            throw new AuthenticateException("USER_DISABLED", e);

        } catch (BadCredentialsException e) {

            throw new AuthenticateException("INVALID_CREDENTIALS", e);

        }

    }
}
