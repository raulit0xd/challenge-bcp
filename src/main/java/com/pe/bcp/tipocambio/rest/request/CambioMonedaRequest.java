package com.pe.bcp.tipocambio.rest.request;

import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class CambioMonedaRequest implements Serializable {

    private BigDecimal monto;
    private String monedaOrigen;
    private String monedaDestino;
    private String fechaCambio;
}
