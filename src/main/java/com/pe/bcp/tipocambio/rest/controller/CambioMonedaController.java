package com.pe.bcp.tipocambio.rest.controller;

import com.pe.bcp.tipocambio.model.dto.CambioMonedaDto;
import com.pe.bcp.tipocambio.rest.request.CambioMonedaRequest;
import com.pe.bcp.tipocambio.service.CambioMonedaService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping("/cambio")
public class CambioMonedaController {

    private CambioMonedaService cambioMonedaService;

    @PostMapping
    public CambioMonedaDto create(@RequestBody CambioMonedaRequest cambioMonedaRequest) {
        return cambioMonedaService.realizarCambio(cambioMonedaRequest);
    }

    @PutMapping
    public ResponseEntity<Void> updateTipoCambio(@RequestBody CambioMonedaRequest cambioMonedaRequest) {
        cambioMonedaService.actualizarTipoCambio(cambioMonedaRequest);
        return ResponseEntity.noContent().build();
    }
}
