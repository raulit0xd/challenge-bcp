package com.pe.bcp.tipocambio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = {
		"com.pe.bcp.tipocambio.model.repository"})
@SpringBootApplication
public class TipoCambioApplication {

	public static void main(String[] args) {
		SpringApplication.run(TipoCambioApplication.class, args);
	}

}
